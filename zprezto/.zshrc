
#
# Executes commands at the start of an interactive session.
#
# Authors:
#   Sorin Ionescu <sorin.ionescu@gmail.com>
#

# Source Prezto.
if [[ -s "${ZDOTDIR:-$HOME}/.zprezto/init.zsh" ]]; then
  source "${ZDOTDIR:-$HOME}/.zprezto/init.zsh"
fi

# Customize to your needs...
#
PATH=$PATH:/usr/local/Caskroom/google-cloud-sdk/latest/google-cloud-sdk/bin

source "${ZDOTDIR:-$HOME}/.zaliases"

if asdf current java > /dev/null 2>&1
then
    export JAVA_HOME=$(asdf where java)
fi

## Golang

export GOPATH=$HOME/go
export GOROOT="$(brew --prefix golang)/libexec"
export PATH="$PATH:${GOPATH}/bin:${GOROOT}/bin"
if which rbenv > /dev/null; then eval "$(rbenv init -)"; fi

## Java
export PATH="/usr/local/opt/openjdk/bin:$PATH"
export PATH="/usr/local/opt/openjdk@11/bin:$PATH"


## Python
export PIP_REQUIRE_VIRTUALENV=true
# export VIRTUAL_ENV_DISABLE_PROMPT=1
# create syspip workaround
syspip2(){
  PIP_REQUIRE_VIRTUALENV=false pip2 "$@"
}
syspip3(){
  PIP_REQUIRE_VIRTUALENV=false pip3 "$@"
}
function virtualenv_info {
    [ $VIRTUAL_ENV ] && echo '('`basename $VIRTUAL_ENV`') '
}

export PATH="/Users/casburnett/Library/Python/3.11/bin:$PATH"
export PYENV_ROOT="$HOME/.pyenv"
export PATH="$PYENV_ROOT/bin:$PATH"
export PYTHON_BIN_PATH="$(python3 -m site --user-base)/bin"
export PATH="$PATH:$PYTHON_BIN_PATH"


if type brew &>/dev/null
then
  FPATH="$(brew --prefix)/share/zsh/site-functions:${FPATH}"

  autoload -Uz compinit
  compinit
fi

. /opt/homebrew/opt/asdf/libexec/asdf.sh
. ~/.asdf/plugins/dotnet/set-dotnet-env.zsh

export PATH="/opt/homebrew/sbin:$PATH"

