# dotfiles



## Getting started

install anywhere
```
sh -c "$(curl -fsSL http://go.stephenroberts.io/dotfiles)" && source ~/.dotfiles/source.sh
```

Don't have **git** installed?

```
cd; curl -#L https://github.com/mathiasbynens/dotfiles/tarball/main | tar -xzv --strip-components 1 --exclude={README.md,bootstrap.sh,.osx,LICENSE-MIT.txt}
```

Already have **git** installed?

```
git clone https://github.com/mathiasbynens/dotfiles.git && cd dotfiles && source bootstrap.sh
```



### references and ripoffs

- https://github.com/mathiasbynens/dotfiles
- https://github.com/webpro/dotfiles
