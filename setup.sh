#!/bin/sh

DOTFILES=$1
BOOTSTRAP=$2

if [ -d "${ZDOTDIR:-$HOME}/.zprezto" ]; then
  cd $ZDOTDIR
  git checkout master
  git pull origin master
else
  git clone --recursive https://github.com/sorin-ionescu/prezto.git "${ZDOTDIR:-$HOME}/.zprezto"
fi


[ ! -r ~/.zlogin ] && ln -s "$HOME/dotfiles/zprezto/.zlogin" ~/.zlogin
[ ! -r ~/.zlogout ] && ln -s "$HOME/dotfiles/zprezto/.zlogout" ~/.zlogout
[ ! -r ~/.zpreztorc ] && ln -s "$HOME/dotfiles/zprezto/.zpreztorc" ~/.zpreztorc
[ ! -r ~/.zshenv ] && ln -s "$HOME/dotfiles/zprezto/.zshenv" ~/.zshenv
[ ! -r ~/.zshrc ] && ln -s "$HOME/dotfiles/zprezto/.zshrc" ~/.zshrc
[ ! -r ~/.zaliases ] && ln -s "$HOME/dotfiles/zprezto/.zaliases" ~/.zaliases
if [ ! -r ~/.zprofile ]; then
    ln -s "$HOME/dotfiles/zprezto/.zprofile" ~/.zprofile
else
    mv $HOME/.zprofile  $HOME/.zprofle_bkup
    ln -s "$HOME/dotfiles/zprezto/.zprofile" ~/.zprofile
fi


if [ ! -r ~/.gitconfig ]; then
    ln -s "$HOME/dotfiles/git/.gitconfig" ~/.gitconfig
else
    ln -s "$HOME/dotfiles/git/.gitconfig" ~/.gitconfig
fi

# [ ! -r ~/.zaliases ] && ln -s "$HOME/dotfiles/zprezto/.zaliases" ~/.zaliases

source $HOME/.zprofile
source $HOME/.zshrc